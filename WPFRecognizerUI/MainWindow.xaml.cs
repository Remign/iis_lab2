﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RecognizerLib.Controllers;

namespace WPFRecognizerUI
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public RecognizerController Recognizer { get; set; }

    public int[] X { get; set; }

    public MainWindow()
    {
      InitializeComponent();

      Recognizer = new RecognizerController("properties.json", "examples.json");
      this.LearnRecognizer();
    }

    public void LearnRecognizer()
    {
      X = Recognizer.LearnRecognizer();
      this.InitializePanel();
    }

    public void InitializePanel()
    {
      for (var i = 0; i < X.Length; i++)
      {
        var checkBox = new CheckBox
        {
          Content = Recognizer.Properties[i],
          DataContext = i
        };

        checkBox.Checked += (sender, args) => X[(int)(sender as CheckBox).DataContext] = 1;
        checkBox.Unchecked += (sender, args) => X[(int)(sender as CheckBox).DataContext] = 0;

        MainPanel.Children.Add(checkBox);
      }
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      this.LearnRecognizer();
    }

    private void Button_Click_1(object sender, RoutedEventArgs e)
    {
      var resultData = new List<string>();
      var indexResult = Recognizer.Recognize(X, resultData);

      MainListBox.ItemsSource = resultData;
      MainListBox.SelectedIndex = indexResult;
    }
  }
}
