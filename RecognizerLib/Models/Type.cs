﻿using System.Collections.Generic;

namespace RecognizerLib.Models
{
  public class Type
  {
    public string Name { get; set; }
    public List<Example> Examples { get; set; }

    public double[] Masses { get; set; }
    public double[] Deltas { get; set; }
  }
}