﻿using System.Collections.Generic;

namespace RecognizerLib.Models
{
  public class Example
  {
    public string Name { get; set; }
    public List<string> Properties { get; set; }
  }
}