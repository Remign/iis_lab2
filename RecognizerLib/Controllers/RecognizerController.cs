﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Type = RecognizerLib.Models.Type;

namespace RecognizerLib.Controllers
{
  public class RecognizerController
  {
    private readonly string _examplesJsonFilePath;

    private readonly string _propertiesJsonFilePath;

    public List<string> Properties { get; set; }

    public List<Type> Types { get; set; }

    public RecognizerController(string examplesJsonFilePath, string propertiesJsonFilePath)
    {
      _examplesJsonFilePath = examplesJsonFilePath;
      _propertiesJsonFilePath = propertiesJsonFilePath;
    }

    public int[] LearnRecognizer()
    {
      if (Properties != null)
      {
        Properties.Clear();
      }

      if (Types != null)
      {
        Types.Clear();
      }

      using (var r = new StreamReader(_propertiesJsonFilePath))
      {
        var json = r.ReadToEnd();
        Types = JsonConvert.DeserializeObject<List<Type>>(json);
      }

      using (var r = new StreamReader(_examplesJsonFilePath))
      {
        var json = r.ReadToEnd();
        Properties = JsonConvert.DeserializeObject<List<string>>(json);
      }

      foreach (var type in Types)
      {
        var tempB = new double[Properties.Count];
        foreach (var index in type.Examples.SelectMany(example => example.Properties.Select(property => Properties.IndexOf(property))))
        {
          tempB[index] += 1D / type.Examples.Count;
        }
        type.Masses = tempB;
      }

      var b = new double[Properties.Count];
      for (var i = 0; i < b.Length; i++)
      {
        b[i] = Types.Sum(t => t.Masses[i]);
      }

      foreach (var type in Types)
      {
        var a = new double[Properties.Count];
        for (var i = 0; i < a.Length; i++)
        {
          a[i] = Math.Abs(type.Masses[i] - b[i]);
        }
        type.Deltas = a;
        Debug.WriteLine(a.ToString());
      }

      var x = new int[Properties.Count];

      return x;
    }

    public int Recognize(int[] x, List<string> resultData)
    {
      resultData.Clear();

      var result = new double[Types.Count];
      var records = new string[Types.Count];

      for (var i = 0; i < Types.Count; i++)
      {
        var sums = new List<double>();
        foreach (var example in Types[i].Examples)
        {
          var sum = 0D;
          for (var j = 0; j < Properties.Count; j++)
          {
            var property = Properties[j];
            var y = example.Properties.Contains(property) ? 1 : 0;
            if (x[j] == y)
            {
              sum += Types[i].Deltas[j];
            }
            else
            {
              sum -= Types[i].Deltas[j];
            }
          }
          sum /= Types[i].Deltas.Sum();
          sum = Math.Max(0, sum);
          sums.Add(sum);
        }

        result[i] = sums.Max();
        records[i] = Types[i].Examples[sums.IndexOf(result[i])].Name;
        resultData.Add(Types[i].Name + " => " + records[i] + " => " + result[i]);
      }

      Debug.WriteLine(result.ToString());
      Debug.WriteLine(records);

      var max = result.Max();
      var index = result.ToList().IndexOf(max);

      Debug.WriteLine(Types[index].Name);

      return index;
    }
  }
}